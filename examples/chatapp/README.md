# chatapp

"small" example app demonstrating the use of local and remote actors.

## usage

set up a hidden service and then run with `cargo run -- <data_dir> <port>` where `data_dir` is a Tor data directory for a hidden service and `port` matches whatever port you set up your hidden service with.
